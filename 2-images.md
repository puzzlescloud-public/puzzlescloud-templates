# Images

> NOTE: Image title (caption) will be mapped to `PC_ImageCaption` docx template style

## Images without Document captions

### Image from Internet

![image](https://raw.github.com/adam-p/markdown-here/master/src/common/images/icon48.png)

### Image with relative path

```
![image](./images/placeholder.png)
```
![image](./images/placeholder.png)

### Image with absolute path

```
![image](/images/placeholder.png)
```
![image](/images/placeholder.png)

## Images with Document captions

### Image with relative path and image title

```
![image](./images/placeholder.png "Image Title")
```

![image](./images/placeholder.png "Image Title")

### Image with absolute path and image title

```
![image](/images/placeholder.png "Image Title")
```

![image](/images/placeholder.png "Image Title")