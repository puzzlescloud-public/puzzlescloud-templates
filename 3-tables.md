# Tables

> **NOTE:** Table text below will be mapped to `PC_TableText` docx template style and Table Title (caption) to `PC_TableCaption` docx template style

Insert tables of the desired sizes with `Insert table` button.

> **NOTE:** For the best user experience, it is suggested to use WYSIWYG editor.

## Table Alignment

| Left Aligned Column | Centered Column | Right aligned Column |
| ------------------- | :-------------: | -------------------: |
| left-aligned | centered | right-aligned |
| left-aligned | centered | right-aligned |
| left-aligned | centered | right-aligned |

## Table with Table Title (Caption)

| Left Aligned Column | Centered Column | Right aligned Column |
| ------------------- | :-------------: | -------------------: |
| left-aligned | centered | right-aligned |
| left-aligned | centered | right-aligned |
| left-aligned | centered | right-aligned |
[ Table Title ]

## Cells Merge

| @cols=2:merged |
| ------ | ------ |
| column 1 | column 2 |


