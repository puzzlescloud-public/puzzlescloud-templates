# Code

> NOTE: All code styles will be mapped to `PC_Code` docx template style, `Code Block` as well as `Inline code`

You can insert CodeBlock style below with `CB` button

```
This is CodeBlock style with text rendering
```

You can also insert `Inline code` style with `</>` button.

You can also use code syntax highlight (works in HTML, but not for a ***Document*** since all syntaxes will be mapped to `PC_Code` docx style.

```js
console.log('This is CodeBlock style with js rendering')
```

```javascript
console.log('This is CodeBlock style with javascript rendering'
```

```html
<div id="editor"><span>This is CodeBlock style with html rendering</span></div>
```

```wrong
[1 2 3]
```

```clojure
[1 2 3]
```