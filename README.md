# Welcome to PuzzlesCloud!

You are at the moment navigated to the project `PuzzlesCloud` and reading its README Article.

This is the example of the PuzzlesCloud application structure:

$$uml

@startmindmap
<style>
node {
    Padding 12
    Margin 3
    HorizontalAlignment center
    LineColor white
    LineThickness 2.0
    BackgroundColor lightgray
    RoundCorner 20
    MaximumWidth 200
}
</style>

+[#yellow] Organisation
++ Team Alpha*
+++[#DodgerBlue] PuzzlesCloud
++++_ README.md
+++[#darkorange] Project 2
++++_ Article 1
++++_ Article 2
++ Team Beta*

--[#5e5adb] <color:white>Main Team (default)</color>
---[#DodgerBlue] PuzzlesCloud
----_ README.md
---[#DodgerBlue] Project 1
----_ Article 1
----_ Article 2
----[#cornflowerblue] Document 1
-----_ Article 1
-----_ Article 2
----[#cornflowerblue] Document 2
-----_ Article 1
-----_ Article 3
-----_ Article 4
---[#darkorange] Project 2
----_ Article 3
----_ Article 4
---[#DarkOrange] Project 3
----[#lightgreen] Webdoc 1
-----_ Article 1
-----_ Article 2
-----_ Article 4

' set legend to have a white background
skinparam legendBackgroundColor #FFFFFF
' remove box around legend
skinparam legendBorderColor #FFFFFF
' remove the lines between the legend items
skinparam legendEntrySeparator #FFFFFF


legend right
'   the <#FFFFFF,#FFFFFF> sets the background color of the legend to white
    <#FFFFFF,#FFFFFF>|<#Dodgerblue>| Cloud projects|
    ' the space between the | and <#blue> is important to make the color column wider
    |<#DarkOrange>     | Git projects|
endlegend

@endmindmap

$$


## How to Create or Import a New Project

1. Create a new Cloud Project -> Click on `Teams & Projects > Create New Project` and fill out necessary fields.
2. Import a new Git Project -> Click on `Teams & Projects > Import New Project` and fill out necessary fields. 

## How to Create a New Article

1. Create a new Article -> Click on `File > New > Article` and fill out necessary fields. 

> **NOTE:** An Article is a single Markdown file that describes a single topic.

## How to Create a New Document

1. Create a new Document -> Click on `File > + > Document` and fill out necessary fields. 

> **NOTE:** A Document is a set of Articles (topics), that are ordered in Document's Table of Contents and that can be exported as an `.docx` or `.pdf`  file in selected predefined docx template.

## How to Create a New Webdoc (Coming Soon)

1. Create a new Article -> Click on `File > + > Webdoc` , fill out necessary fields and execute necessary steps. 

> **NOTE:** Webdoc is a structure similar to a Document with Table of Contents that could be published as a documentation website. 

## How to Create a New Team (Coming Soon)

1. Create a new Team -> Click on `Team Icon (upper right corner) > Create new team` and fill out necessary fields. 
2. Add Team members (Optional) -> Click on `Team Icon (upper right corner) > Team Settings` and fill out necessary fields. 