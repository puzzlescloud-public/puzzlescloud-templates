# About C4 

## The C4 model is...
1. A set of hierarchical abstractions (software systems, containers, components, and code).
2. A set of hierarchical diagrams (system context, containers, components, and code).
3. Notation independent.
4. Tooling independent.

## Uses and benefits
The C4 model is an easy to learn, developer friendly approach to software architecture diagramming. Good software architecture diagrams assist with communication inside/outside of software development/product teams, efficient onboarding of new staff, architecture reviews/evaluations, risk identification (e.g. risk-storming), threat modelling, etc.

![C4 Model Overview](https://c4model.com/images/c4-overview.png "C4 Model Overview")

More information about [C4 Model](https://c4model.com/)

