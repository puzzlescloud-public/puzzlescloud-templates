# System Context

- A System Context diagram is a good starting point for diagramming and documenting a software system, allowing you to step back and see the big picture. Draw a diagram showing your system as a box in the centre, surrounded by its users and the other systems that it interacts with.

- Detail isn't important here as this is your zoomed out view showing a big picture of the system landscape. The focus should be on people (actors, roles, personas, etc) and software systems rather than technologies, protocols and other low-level details. It's the sort of diagram that you could show to non-technical people.

**Scope:** A single software system.

**Primary elements:** The software system in scope.
Supporting elements: People (e.g. users, actors, roles, or personas) and software systems (external dependencies) that are directly connected to the software system in scope. Typically these other software systems sit outside the scope or boundary of your own software system, and you don't have responsibility or ownership of them.

**Intended audience:** Everybody, both technical and non-technical people, inside and outside of the software development team.

**Recommended for most teams:** Yes.

$$uml
@startuml
set separator none
title Internet Banking System - System Context

top to bottom direction

!include <C4/C4>
!include <C4/C4_Context>

AddBoundaryTag("Big Bank plc", $borderColor="#cccccc", $fontColor="#cccccc")
Boundary(group_1, "Big Bank plc", $tags="Big Bank plc") {
  System(MainframeBankingSystem, "Mainframe Banking System", $descr="Stores all of the core banking information about customers, accounts, transactions, etc.", $tags="", $link="")
  System(EmailSystem, "E-mail System", $descr="The internal Microsoft Exchange e-mail system.", $tags="", $link="")
  System(InternetBankingSystem, "Internet Banking System", $descr="Allows customers to view information about their bank accounts, and make payments.", $tags="", $link="")
}

Person(PersonalBankingCustomer, "Personal Banking Customer", $descr="A customer of the bank, with personal bank accounts.", $tags="", $link="")

Rel_D(PersonalBankingCustomer, InternetBankingSystem, "Views account balances, and makes payments using", $techn="", $tags="", $link="")
Rel_D(InternetBankingSystem, MainframeBankingSystem, "Gets account information from, and makes payments using", $techn="", $tags="", $link="")
Rel_D(InternetBankingSystem, EmailSystem, "Sends e-mail using", $techn="", $tags="", $link="")
Rel_D(EmailSystem, PersonalBankingCustomer, "Sends e-mails to", $techn="", $tags="", $link="")

SHOW_LEGEND(true)
@enduml
$$

