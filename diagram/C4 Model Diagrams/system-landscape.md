# System Landscape

The C4 model provides a static view of a single software system but, in the real-world, software systems never live in isolation. For this reason, and particularly if you are responsible for a collection/portfolio of software systems, it's often useful to understand how all of these software systems fit together within a given enterprise, organisation, department, etc. Essentially this is a map of the software systems within the chosen scope, with a C4 drill-down for each software system of interest.

From a practical perspective, a system landscape diagram is really just a system context diagram without a specific focus on a particular software system.

**Scope:** An enterprise/organisation/department/etc.
**Primary elements:** People and software systems related to the chosen scope.
**Intended audience:** Technical and non-technical people, inside and outside of the software development team.

$$uml
@startuml
set separator none
title System Landscape

top to bottom direction

!include <C4/C4>
!include <C4/C4_Context>

AddBoundaryTag("Big Bank plc", $borderColor="#cccccc", $fontColor="#cccccc")
Boundary(group_1, "Big Bank plc", $tags="Big Bank plc") {
  Person(CustomerServiceStaff, "Customer Service Staff", $descr="Customer service staff within the bank.", $tags="", $link="")
  Person(BackOfficeStaff, "Back Office Staff", $descr="Administration and support staff within the bank.", $tags="", $link="")
  System(MainframeBankingSystem, "Mainframe Banking System", $descr="Stores all of the core banking information about customers, accounts, transactions, etc.", $tags="", $link="")
  System(EmailSystem, "E-mail System", $descr="The internal Microsoft Exchange e-mail system.", $tags="", $link="")
  System(ATM, "ATM", $descr="Allows customers to withdraw cash.", $tags="", $link="")
  System(InternetBankingSystem, "Internet Banking System", $descr="Allows customers to view information about their bank accounts, and make payments.", $tags="", $link="")
}

Person(PersonalBankingCustomer, "Personal Banking Customer", $descr="A customer of the bank, with personal bank accounts.", $tags="", $link="")

Rel_D(PersonalBankingCustomer, InternetBankingSystem, "Views account balances, and makes payments using", $techn="", $tags="", $link="")
Rel_D(InternetBankingSystem, MainframeBankingSystem, "Gets account information from, and makes payments using", $techn="", $tags="", $link="")
Rel_D(InternetBankingSystem, EmailSystem, "Sends e-mail using", $techn="", $tags="", $link="")
Rel_D(EmailSystem, PersonalBankingCustomer, "Sends e-mails to", $techn="", $tags="", $link="")
Rel_D(PersonalBankingCustomer, CustomerServiceStaff, "Asks questions to", $techn="Telephone", $tags="", $link="")
Rel_D(CustomerServiceStaff, MainframeBankingSystem, "Uses", $techn="", $tags="", $link="")
Rel_D(PersonalBankingCustomer, ATM, "Withdraws cash using", $techn="", $tags="", $link="")
Rel_D(ATM, MainframeBankingSystem, "Uses", $techn="", $tags="", $link="")
Rel_D(BackOfficeStaff, MainframeBankingSystem, "Uses", $techn="", $tags="", $link="")

SHOW_LEGEND(true)
@enduml
$$