# Chart Diagram

$$chart
,blocked,not applicable,failed,passed,total
v1.0.0,7,61,94,572,734
v1.1.0,31,17,94,572,714

type: column
title: Test Report
x.title: Release
y.title: Test Cases Executed
y.min: 1
y.max: 40
y.suffix: Number
$$