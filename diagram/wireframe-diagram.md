# Wireframe Diagram

## Basic Widgets

$$uml
@startsalt
{
  Just plain text
  [This is my button]
  ()  Unchecked radio
  (X) Checked radio
  []  Unchecked box
  [X] Checked box
  "Enter text here   "
  ^This is a droplist^
}
@endsalt
$$

## Adding Tabs

```
$$uml
@startsalt
{+
{/ <b>General | Fullscreen | Behavior | Saving }
{
{ Open image in: | ^Smart Mode^ }
[X] Smooth images when zoomed
[X] Confirm image deletion
[ ] Show hidden images
}
[Close]
}
@endsalt
$$
```

$$uml
@startsalt
{+
{/ <b>General | Fullscreen | Behavior | Saving }
{
{ Open image in: | ^Smart Mode^ }
[X] Smooth images when zoomed
[X] Confirm image deletion
[ ] Show hidden images
}
[Close]
}
@endsalt
$$

## Login Example

```
$$uml
@startsalt
{^"My Login"
  <&person> User  | "MyName   "
  <&key> Password  | "****     "
  [<&circle-x> Cancel ] | [ <&account-login> OK   ]
}
@endsalt
$$
```

$$uml
@startsalt
{^"My Login"
  <&person> User  | "MyName   "
  <&key> Password  | "****     "
  [<&circle-x> Cancel ] | [ <&account-login> OK   ]
}
@endsalt
$$

## Icons Supported

```
$$uml
@startuml
listopeniconic
@enduml
$$
```

$$uml
@startuml
listopeniconic
@enduml
$$

More examples of the [Wireframe Diagrams](https://plantuml.com/salt)