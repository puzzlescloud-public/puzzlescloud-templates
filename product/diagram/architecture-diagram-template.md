# Architecture Diagram Template


$$uml
@startuml
skinparam packageStyle rectangle
skinparam componentStyle rectangle
skinparam actorStyle awesome
skinparam component {
  FontSize 13
  BackgroundColor<<Apache>> Pink
  BorderColor<<Apache>> #FF6655
  FontName Courier
  BorderColor black
  BackgroundColor Orange
  ArrowFontName Impact
  ArrowColor #FF6655
  ArrowFontColor #777777
}
interface "HTTP" as http

[i]
component Component1 {
  portin p1
  portin p2
  portin p3
  portout po1
  portout po2
  portout po3
  component [blue component] #DodgerBlue
}
[o]
i --> p1
i --> p2
i --> p3
p2 --> [blue component] 
po1 --> o
po2 --> o
po3 --> o
[blue component]  --> po1

package "Package Group" {
   [First Component] #DodgerBlue
  http - [First Component]
  (Green Component) #LimeGreen
  [First Component] <-right-> (Green Component) 
}

node "Node Group" {
  interface "FTP" as ftp
  component second [
  this is two raws
  second component
  ]
  ftp -right-> [second]
  [First Component] -down-> ftp
}

cloud "Cloud Group" {
  [Cloud Component]
}

database "Database Group" {
  folder "Folder" {
    [Folder 3]
  }
  frame "Frame" {
     [Component inside Frame]
  }
}
:user: -right-> http
note top of user : This is a top note. Bottom, left and right are also possible
[Green Component] --> [Cloud Component]
[Cloud Component] --> [Folder 3]
[Folder 3] --> [i]

@enduml

$$