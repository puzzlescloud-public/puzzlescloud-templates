# Epics/User Stories/Tickets Structure

$$uml
@startuml PERT
left to right direction
' Horizontal lines: -->, <--, <-->
' Vertical lines: ->, <-, <->
title Title: Ticketing Structure

map Epic.1 {
}
map Epic.2 {
}
map Epic.3 {
}
map UserStory.1 {
}
map UserStory.2 {
}
map UserStory.3 {
}
map task.1 {
    Start => End
}
map task.2 {
    Start => End
}
map task.3 {
    Start => End
}
map subtask.1 {
    Start => End
}
map subtask.2 {
    Start => End
}
map subtask.3 {
    Start => End
}
Epic.3 --> UserStory.1 : Brake down
Epic.3 --> UserStory.2 : Brake down
Epic.3 --> UserStory.3 : Brake down
UserStory.3--> task.1 : Brake down
UserStory.3 --> task.2 : Brake down
UserStory.3 --> task.3 : Brake down
task.3 --> subtask.1 : Brake down
task.3 --> subtask.2 : Brake down
task.3 --> subtask.3 : Brake down
@enduml
$$