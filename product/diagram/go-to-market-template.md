# Go to Market Strategy

$$uml

@startmindmap

<style>
node {
    Padding 12
    Margin 12
    HorizontalAlignment center
    LineColor darkgreen
    BackgroundColor lightgreen
    RoundCorner 40
    MaximumWidth 150
}
</style>

*[#DodgerBlue] GTM
++_ Your motto 
** Values Add Propositions
***_ Value 1
***_ Value 2
***_ Value 3
** SaaS 
*** Subscription
****_ Package 1
*****_ 1 Team
******_ 5 users
****_ Package 2
*****_ 2 Teams
******_ > 10 Users
*****_ ...
*****_ ∞
*** ICP
****_  Company profile
*****_ Department 1
*****_ Department 2
****_ Roles
*****_ CEO
*****_ CTO
*** Inbound
**** Google Ads
*****_ Approach 1
*****_ Approach 2
*****_ Approach 3
**** Google Display 
*****_ Website 1
*****_ Website 2
*****_ Website 3
**** YouTube Video
*****_ Scenario
*****_ Recording & editing
**** LN
*****_ Campaign
*****_ Posts
**** Google Native Search
**** Press Releases
**** Other
*** Outbound
****_ Email
*****_ Follow up 1 Week
******_ Follow up 1 Week
****_ LinkedIn
*****_ Contact request
******_ Message 
*******_ follow up
** Channel Partners
***_ Resellers
***_ Other partners

@endmindmap

$$

