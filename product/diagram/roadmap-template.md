# Roadmap Template
## SW Releases
$$uml
@startgantt
projectscale quarterly
hide footbox
<style>
ganttDiagram {
	task {
		FontColor White
		FontSize 16
		FontStyle bold
		BackGroundColor #32CD32
		LineColor #32CD32
	}
	milestone {
		FontColor black
		FontSize 16
		FontStyle italic
		BackGroundColor orange
		LineColor Blue
	}
	note {
		FontColor DodgerBlue
		FontSize 18
		LineColor DodgerBlue
	}
	arrow {
		FontName Helvetica
		FontColor red
		FontSize 18
		FontStyle bold
		BackGroundColor plum
		LineColor blue
		LineStyle 8.0;13.0
		LineThickness 3.0
	}
	separator {
		BackgroundColor GainsBoro
		LineStyle 8.0;3.0
		LineColor Blue
		LineThickness 1.0
		FontSize 16
		FontStyle bold
		FontColor DodgerBlue
		Margin 15
		Padding 10
	}
	timeline {
	    BackgroundColor DodgerBlue
            FontName Arial
	    FontColor white
             

	}
	closed {
		BackgroundColor MistyRose
		FontColor GainsBoro
                
	}
}
</style>
Project starts the 2021-07-01
[Incorporation] happens at 2021-10-20
[Task 1] starts 2021-11-05
[Task 1] lasts 240 days
[MVP] happens at 2022-03-31
[GitLab Tech Alliance Partner] happens at 2022-09-08
[Task 2] starts 2022-10-01
[On-premise launch] happens at 2023-04-01
[Task 2] lasts 360 days
-- Phase I --
[Task 3] starts 2024-03-15
[PuzzlesCloud 4.0 launch] happens at 2024-06-01
[ISO 27001 certification] happens at 2024-06-01
[Task 3] requires 360 days
[Task 3] is coloured in Orange
[Task 3] is 55% complete
[Task 4] starts 2024-10-01
[Task 4] lasts 810 days
[Task 4] is coloured in DodgerBlue/DodgerBlue
note bottom
Features:
  - Feature 1
  - Feature 2 
  - Prompt engineering
end note
[PuzzlesAI launch] happens at 2025-03-31
[Milestones Achieved] starts 2026-10-01
note bottom
  - Milestone 1
  - Milestone 2
  - Milestone 3
end note
[Milestones Achieved] ends 2028-02-01
<style>
	separator {
		Margin 1
		Padding 0
	}
</style>
@endgantt
$$
