# Team Structure

$$uml

@startwbs

*[#Green] Research & Development
**[#DodgerBlue] PRODUCT 1
*** Product Owner
****[#Orange] Scrum Team
***** Scrum Master
***** Technical Lead
***** Frontend Developer
***** Backend Developer
***** DevOps
***** Test Developer
**[#DodgerBlue] PRODUCT 2
*** Product Owner
****[#Orange] Scrum Team
***** Scrum Master
***** Technical Lead
***** Frontend Developer
***** Backend Developer
***** DevOps
***** Test Developer

@endwbs

$$