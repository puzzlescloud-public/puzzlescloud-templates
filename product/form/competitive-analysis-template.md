# Competitive Analysis


| Field | Value  |
| --- | --- |
| Competitors |  |
| Created by|  |
| Date Created|  |

## Competitors Profile

| Company | Company-1 | Company-2 | Company-3 |
| --- | --- | --- |--- |
| Company mission |  |  |. |
| Key Objectives|  |  |. |
| Capabilities|  |  |. |
| Company size|  |  |. |
| Revenue|  |  |. |

## Competitive Advantage

* Key differentiators

## Market

| Company | Company-1 | Company-2 | Company-3 | my Product|
| --- | --- | --- |--- |--- |
| Target market |  |  |. |
| Verticals|  |  |. |
| Market share|  |  |. |
| Company size|  |  |. |
| Revenue|  |  |. |

## Product

| Product| Company-1 | Company-2 | Company-3 | my Product|
| --- | --- | --- |--- |--- |
| Product overview |  |  |. |
| Positioning/Category  |  |. |
| Pricing|  |  |. |
|Product portals listing |  |  |. |
| |  |  |. |

---

| Core features| Company-1 | Company-2 | Company-3 | my Product|
| --- | :---: | :---:  |:---: |:---: |
| Feature 1 | [ x ] | [ ✓ ] | [ ✓ ]| [ ✓ ]|
| Feature 2  |  |. |
| Feature 3|  |  |. |
| |  |  |. |
| |  |  |. |

## Marketing Strategy

| Marketing| Company-1 | Company-2 | Company-3 | my Product|
| --- | --- | --- |--- |--- |
| Overall strategy|  |  |. |
| Website  |  |. |
| Blog/content|  |  |. |
|Social networks presence |  |  |. |
|Online advertising |  |  |. |
|Offline advertising |  |  |. |
|Videos and Webinars |  |  |. |
|Major events |  |  |. |
|Customer resources|  |  |. |
|Customer review listings |  |  |. |
|Press releases and mentions |  |  |. |
|Customer engagement |  |  |. |
|Social proof |  |  |. |
|Partnerships and investments |  |  |. |

## SWOT Analysis

| SWOT| Company-1 | Company-2 | Company-3 | my Product|
| --- | --- | --- |--- |--- |
| Strengths |  |  | |
| Weaknesses  |  | |
| Opportunities|  |  ||
| Threads |  |  | |




