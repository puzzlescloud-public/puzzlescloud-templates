# Lean Canvas Template

## Purpose

The Lean Canvas model helps deconstruct a business idea into its key assumptions. Deeply influenced by the lean methodology, the Lean Canvas servers as a tactical plan to guide a business from ideation through build.


## Canvas

|Problem  | Solution | Value Add Proposition | Unfair Advantage | Customer Segment| 
| --- | --- | --- |--- |--- |
| List top 3 problems |  Top 3 features| Single, clear and compelling message that states why you are different and worth buying| Can’t be easily copied or bought | Target Customers|
---
|Existing Alternatives  | Key Metrics| High-Level Concept | Channels | Early Adopters| 
| --- | --- | --- |--- |--- |
| List how these problems are solved today.|  Key activities you measure| List your X for Y analogy (e.g. YouTube = Flickr for videos) | Path to customers | List the characteristics of your ideal customers.|
---

| Cost Structure  | Revenue Streams  |
| --- | --- |
| List your fixed and variable costs Customer acquisition costs Distribution costs Hosting People Etc...  | List your sources of revenue Revenue model Life time 


