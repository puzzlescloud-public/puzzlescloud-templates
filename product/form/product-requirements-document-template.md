# Product Requirements Document

## Title

Give this project a distinct name.


## Details

| Details |  | 
| :--------| ----- | 
| Document Status | Draft | 
| Document Owner | John Doe | 

## Change History

Change History: Describe each important change to the PRD, including who changed it, when they changed it, and what they changed.

| Date | Role | Person | Description |
| :--------: | ----- | :-----: |:-----: |
| 01.01.2022 | Product Owner | John Doe | Description of the change |
| 01.01.2022 | Product Owner | John Doe | Description of the change |
| 01.01.2022 | Product Owner | John Doe | Description of the change |

## Overview

Briefly, what is this project about?  Why are you doing it?

## Success Metrics

What are the success metrics that indicate you’re achieving your internal goals for the project?

## Customer Messaging

What’s the product messaging marketing will use to describe this product to customers, both new and existing?

## RoadmapTimeline/Release Planning

Reference to what’s the overall schedule you’re working towards?

## Target Personas

Who are the target personas for this product, and which is the key persona  (ideal customer persona)?

## User Scenarios

These are full stories about how various personas will use the product in context.

## Assumptions

Anything that might impact product development positively or negatively, along with how you will validate, and any known dependencies.

## User Stories/Features/Requirements

Include references to distinct, prioritised features along with a short explanation as to why this feature is important.

## Out of Scope

What have you explicitly decided not to do and why

## Designs

Include references to any needed early sketches, and throughout the project, link to the actual designs once they’re available.

## Open Questions

 What factors do you still need to figure out?

## Q&A

What are common questions about the product along with the answers you’ve decided? This is a good place to note key decisions.

## Other Considerations

 This is a catch-all for anything else, such as if you make a key decision to remove or add to the project’s scope.