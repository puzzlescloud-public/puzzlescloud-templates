# Product Strategy Template

## Details

| Details |  | 
| :--------| ----- | 
| Document Status | Draft | 
| Document Owner | John Doe | 

## Change History

Change History: Describe each important change to the PRD, including who changed it, when they changed it, and what they changed.

| Date | Role | Person | Description |
| :--------: | ----- | :-----: |:-----: |
| 01.01.2022 | Product Owner | John Doe | Description of the change |
| 01.01.2022 | Product Owner | John Doe | Description of the change |
| 01.01.2022 | Product Owner | John Doe | Description of the change |



## Company Overview

Brief description of your company, its main products or services, and any other valuable strategic information.

## Objectives

> 1. Reduce Churn
> 2. [description]

---
An Objective is a qualitative statement outlining a high level goal your product is set to achieve. These are typically intentionally broad and speak to the team’s long-term mission.

## Key Results

> 1. Increase average deal size by 50%
> 2. [description]

---
Specific and actionable, Key Results reflect the actions you want to take to achieve your objective. They are usually a numerical metric for growth, performance, or engagement, but they can also represent a milestone that is done or undone. 

## Competitive Landscape

Competitive landscape table provides an overall market outlook and helps assess your next steps for your brand and business

Please check `Competitive Analysis Template`.

## Lean Canvas

The Lean Canvas model helps deconstruct a business idea into its key assumptions. Deeply influenced by the lean methodology, the Lean Canvas servers as a tactical plan to guide a business from ideation through build.

Please check `Lean Canvas Template`.





