# Product Vision Template

**For** (target customer)
**Who** (statement of need or opportunity)
**The** (product name) **is a** (product category)
**That** (key benefit, reason to buy)
**Unlike** (primary competitive alternative)
**Our product** (statement of primary differentiation)

## Product Vision Example


**For** technical teams
**Who** wish to be more effective
**The** PuzzlesCloud **is an** innovative tech documentation solution
**That** utilises software development workflows also for documentation
**Unlike** other documentation solutions which do not support native development workflows
**Our product** resolves scattered information, version control, diverse communication/collaboration workflows and outdated documentation issues
