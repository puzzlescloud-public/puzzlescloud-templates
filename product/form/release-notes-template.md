# Release Notes Template

Release date: 31.12.2022

### Component Versions

<!-- Please include here all component versions used in the test.  -->
- UI: 1.0.0
- MySQL: 1.2.3
- etc


### Features

| Issue ID | Title | State |
| :--------: | ----- | :-----: |
| 308 | Description | Closed |
| 309 | Description | Closed |
| 348 | Description | Closed |
| 349 | Description | Closed |
| 350 | Description | Closed |

### Bug Fixes

| Issue ID | Title | State |
| :--------: | ----- | :-----: |
| 40 | Description | Closed |
| 93 | Description | Closed |
| 103 | Description | Closed |