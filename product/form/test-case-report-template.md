# Test Case Definition & Report Template

## Test Name & ID
<!-- Please include here a self descriptive and explanatory name of the test. Test name should be assigned by the engineer in charge of Test Case creation and should be self-explanatory to identify goal -->
Test application login
login_001
## Author(s)
<!-- Name of test engineer(s) that have created the Test Case. -->
John Doe (jdoe@puzzlescloud.com)
## Creation Date
<!-- Please add the date the test was created. Format DD.MM.YYYY-->
31.12.2022
## Execution Date
<!-- Please add the date the test was executed. Format DD.MM.YYYY-->
31.12.2022
## Component Versions
<!-- Please include here all component versions used in the test.  -->
- UI: 1.0.0
- Database: 1.2.3
## Feature Reference
<!-- Add reference to the feature that this test is created for, if applicable -->
[Link to feature specification ](puzzlescloud.com)
## Objective
<!-- Please concisely describe here the technical test goal and objective -->
Make sure that login is functional and has no site-effects 
## Priority
<!-- Please identify here the priority from the business impact point of view -->
- [ ] High
- [x] Medium
- [ ] Low

## Additional Tools Required
<!-- Please identify here if any additional tools are required for this test execution. If yes specify which  -->
- [ ] Yes
- [x] No

## Execution Method
<!-- Please identify here if this test case has been automated -->
- [ ] Automated
- [x] Manual

## Execution Environment
<!-- Please identify here the setup where the test has been executed. -->
- [ ] Dev
- [x] Test
- [ ] Staging
- [ ] Pre-prod

## Device or Component Under Test
<!-- Please identify the component(s) under test. -->
- [x] UI
- [x] Database
- [ ] Other

## Disruptiveness
<!-- Please identify here the impact and disruptiveness to other Test Cases or setup  -->
- [ ] High
- [ ] Medium
- [x] Low
- [ ] No

## Testing Diagram
<!-- Please include here any kind of reference diagrams relevant for the Test Case -->
$$uml
@startuml
left to right direction
skinparam packageStyle rectangle
skinparam componentStyle rectangle
skinparam actorStyle awesome
interface "HTTPS" as https

rectangle App {
  database "Database" as DB
  (UI) -- [DB]
}
:user: --> https
https <--> (UI)
note bottom of user : This is a bottom note.
note top of user : This is a top note.
@enduml

$$

## Prerequisite
<!-- Please include here prerequisite from User Story Template -> Scenario -> Given. This field is limited to the combination of technical prerequisites. -->
I am on the sign-in page and cache in the browser is clean. 

## Configuration
<!-- Please include here JUST the representative configuration snippet that is relevant for this Test Case, if applicable -->
Conf file snippet
```
Timeout 300 5
KeepAlive On
```
or json format using syntax highlighting
```json
{
 "timeout": ["300", "5"],
 "KeepAlive": "On"
}
```
or visual json representation
$$uml
@startjson
{
 "timeout": ["300", "5"],
 "KeepAlive": "On"
}
@endjson
$$
# Procedure
<!-- Please include here a technical description of test procedure) -->
- [x] Check if application is available
example of bash command syntax highlight
```bash
curl -X GET http://10.0.0.1:9000/state -H 'Content-Type: application/json' -H 'X-Auth-Type: type' -H 'X-Auth-Token: token'

"SUCCESS"
```
- [x] Visit the application by a browser
- [x] Submit user credentials
- [x] Click login button

# Expected Results
<!-- Please include here a technical overview of expected results, can be amended after testing -->
- [x] Login to the app was successful 

# Test Results
<!-- Please describe here final Test Results in a summarised fashion -->
- [x] Pass
- [ ] Fail
- [ ] Skip

# Test Output
<!-- Please do not include here all log items, but just those snippets and logs representative for the Test. These section should be in the order of lines, not longer. -->
The following log entry was created by the application
|Timestamp|Severity|Host|Details|
|---|---|---|---|
|2/28/2023 07:42:48|warning|UI| Login successful|

