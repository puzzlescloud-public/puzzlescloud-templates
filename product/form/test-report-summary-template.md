# Test Report Template

## Overview

Textual report overview.

## Manual Test Results

| Category | State | Test Cases |
|:---: | --- | :---: |
| 1.| Passed | 572 |
| 2. | Failed | 94|
| 3. | Not Applicable | 61 |
| 4. | Blocked | 7|
|  | Total | 734 |

## Automated Test Results

| Category | State | Test Cases |
|:---: | --- | :---: |
| 1.| Passed | 572 |
| 2. | Failed | 94|
| 3. | Not Applicable | 61 |
| 4. | Blocked | 7|
|  | Total | 734 |


$$chart
,blocked,not applicable,failed,passed,total
Manual,7,61,94,572,734
Automated,31,17,94,572,714

type: column
title: Test Report
x.title: Release
y.title: Test Cases Executed
y.min: 1
y.max: 40
y.suffix: Number
$$

## Failing Tests Detailed Report

| Priority | Manual Failing Test Bug ID | Automated Failing Test Bug ID |
|:---: | --- | :---: |
| 1.|  |  |
| 2. | 18150, 18406, 18798, 19075, 21290, 22634, 24802, 26203, 27476, 29200, 29219, 29807,29869, 30484, 33298, 33799, 34279, 34313 | 15872,  18150, 18786, 18976, 23626, 25992, 27481, 27676, 27706|
| 3. | 10838, 10842, 16357, 18718,19056, 21800, 29793, 29968,30250, 30137, 30241,30261,30341, 30473, 31960, 32906, | 18977, 20541, 30150 |
| 4. | 29729, 30509, 34249 | 30084|