# User Story Template

- As a `type of user`
- I want `goal/functionality`
- So that I `benefit / desired result`

**Example**

- As a user 
- I want to sign in 
- so I can see my dashboard

### Non functional requirements

Messages and similar

### Acceptance Criteria

1. Scenario
2. Call sequence diagram (PlantUML)
3. UI Design 
4. Unit tests passed
5. Functional tests passed
6. Not affected dependencies

#### Scenario

- Scenario: (Scenario description)
- Given (beginning or precondition)
- When (action taken)
- Then (Result)

**Example**

- **Scenario:** User supplies correct user name and password
- **Given** that I am on the sign-in page
- **When** I enter my user name and password correctly
- **And** click ‘Sign In’
- **Then** I am taken to the dashboard